import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './modulos/home/menu/menu.component';
import { NavbarComponent } from './modulos/home/navbar/navbar.component';
import { HomeComponent } from './modulos/home/home/home.component';
import { ClienteComponent } from './modulos/registros/cliente/cliente.component';
import { ArticulosComponent } from './modulos/registros/articulos/articulos.component';
import { ProveedoresComponent } from './modulos/registros/proveedores/proveedores.component';
import { VentaclienteComponent } from './modulos/ventas/ventacliente/ventacliente.component';
import { GastosComponent } from './modulos/consultas/gastos/gastos.component';
import { ArticulostotComponent } from './modulos/consultas/articulostot/articulostot.component';
import { HttpClientModule } from '@angular/common/http';
import { ClienteformComponent } from './modulos/registros/clienteform/clienteform.component';
import { ArticuloformComponent } from './modulos/registros/articuloform/articuloform.component';
import { ProveedorformComponent } from './modulos/registros/proveedorform/proveedorform.component';
import { FormsModule} from '@angular/forms';
import { VentadetfacturaComponent } from './modulos/ventas/ventadetfactura/ventadetfactura.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    NavbarComponent,
    HomeComponent,
    ClienteComponent,
    ArticulosComponent,
    ProveedoresComponent,
    VentaclienteComponent,
    GastosComponent,
    ArticulostotComponent,
    ClienteformComponent,
    ArticuloformComponent,
    ProveedorformComponent,
    VentadetfacturaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
