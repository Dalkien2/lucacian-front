import { ArticuloService } from './../../../core/servicios/articulo.service';
import { Component, OnInit, Optional, Host } from '@angular/core';
import { ArticulostotComponent } from '../../consultas/articulostot/articulostot.component';
import { ArticuloInt } from 'src/app/core/modelos/articulo-int';
import { ProveedorService } from 'src/app/core/servicios/proveedor.service';
import { ProveedorInt } from 'src/app/core/modelos/proveedor-int';
import { ArticulosComponent } from '../articulos/articulos.component';

@Component({
  selector: 'app-articuloform',
  templateUrl: './articuloform.component.html',
  styleUrls: ['./articuloform.component.css']
})
export class ArticuloformComponent implements OnInit {

  articuloCrear : ArticuloInt = {codArticulo:null, cifProveedor: null, nombreArticulo:"", caracteristica:"", precio:0};
  activado: boolean = true;
  
  listProvedores: ProveedorInt [] = [];
  constructor(@Optional() @Host() private articulos: ArticulosComponent,
   private servArticulo: ArticuloService, private serProveedor: ProveedorService ) { }

  ngOnInit(): void {
    this.serProveedor.obtieneProveedores().subscribe(
      data => this.listProvedores = data as ProveedorInt[], error => alert("contacte al administrador")
    );
  }


  crearArticulo(){
    this.validaCampos();
    if(this.activado){
      console.log(JSON.stringify(this.articuloCrear));
      this.servArticulo.crearArticulo(this.articuloCrear).subscribe(()=> {
        this.articulos.ActualizaLista();});
    }else{
      setTimeout(()=> this.inactivo() ,3000) ;
    }
  }

  inactivo(){
    this.activado = true;
  }
  validaCampos(){
    console.log(this.activado);
    this.activado = this.articuloCrear.caracteristica!=="" && this.articuloCrear.cifProveedor!==0 && 
        this.articuloCrear.nombreArticulo!=="" && this.articuloCrear.precio!==0; 
    console.log(this.activado);
  }

  selProvedor(e){
    this.articuloCrear.cifProveedor = e.target.value ;
    console.log(this.articuloCrear.cifProveedor);
  }
}
