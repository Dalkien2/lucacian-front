import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticuloformComponent } from './articuloform.component';

describe('ArticuloformComponent', () => {
  let component: ArticuloformComponent;
  let fixture: ComponentFixture<ArticuloformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticuloformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticuloformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
