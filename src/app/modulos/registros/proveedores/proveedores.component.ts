import { Component, OnInit } from '@angular/core';
import { ProveedorInt } from 'src/app/core/modelos/proveedor-int';
import { ProveedorService } from 'src/app/core/servicios/proveedor.service';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.css']
})
export class ProveedoresComponent implements OnInit {

  listProvee: ProveedorInt[] = []; 
  constructor( private serProvee: ProveedorService) { }

  ngOnInit(): void {
    this.serProvee.obtieneProveedores().subscribe((data) => 
    this.listProvee = data as ProveedorInt[] , 
    (error)=> alert("contacte al administrador del sistema") );
  }

  ActualizaLista(){
    this.serProvee.obtieneProveedores().subscribe(
      (data) => {this.listProvee = data as ProveedorInt[];},
       (error) => alert("contacte al administrador")
    );
    }

}
