import { ArticuloService } from './../../../core/servicios/articulo.service';
import { Component, OnInit } from '@angular/core';
import { ArticuloInt } from 'src/app/core/modelos/articulo-int';

@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.css']
})
export class ArticulosComponent implements OnInit {

  listArticulo: ArticuloInt[] = []; 
  constructor(private servArticulo: ArticuloService) { }

  ngOnInit(): void {
    this.servArticulo.obtenerArticulos().subscribe(data=> this.listArticulo = data as ArticuloInt[],
       error => alert("contacte con administrador"));
  }

  ActualizaLista(){
    this.servArticulo.obtenerArticulos().subscribe(data=> this.listArticulo = data as ArticuloInt[],
      error => alert("contacte con administrador"));
  }

}
