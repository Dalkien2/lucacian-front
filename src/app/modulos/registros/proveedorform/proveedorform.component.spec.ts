import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProveedorformComponent } from './proveedorform.component';

describe('ProveedorformComponent', () => {
  let component: ProveedorformComponent;
  let fixture: ComponentFixture<ProveedorformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProveedorformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProveedorformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
