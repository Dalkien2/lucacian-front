import { Component, OnInit, Optional, Host } from '@angular/core';
import { ProveedorInt } from 'src/app/core/modelos/proveedor-int';
import { ProveedorService } from 'src/app/core/servicios/proveedor.service';
import { ProveedoresComponent } from '../proveedores/proveedores.component';

@Component({
  selector: 'app-proveedorform',
  templateUrl: './proveedorform.component.html',
  styleUrls: ['./proveedorform.component.css']
})
export class ProveedorformComponent implements OnInit {

  proveedorCrear: ProveedorInt = {cifProveedor:null , nombre:"", apellido1:"", apellido2:"", nombreEmpresa:"", direccion:"" ,cp:"", ciudad:"",provincia:"", pais:"" };
  activado:boolean = true;
  constructor(private serProvee:ProveedorService,
    @Optional() @Host() private proveedor: ProveedoresComponent ) { }

  ngOnInit(): void {
  }

  crearProveedor(){
    this.validaCampos();
    if(this.activado){
      console.log(JSON.stringify(this.proveedorCrear));
      this.serProvee.creaProveedor (this.proveedorCrear).subscribe((data)=> this.proveedor.ActualizaLista());
    }else{
      setTimeout(()=> this.inactivo() ,3000) ;
    }
  }

  inactivo(){
    this.activado = true;
  }
  validaCampos(){
    console.log(this.activado);
    this.activado = this.proveedorCrear.nombre!=="" && this.proveedorCrear.apellido1!=="" &&
        this.proveedorCrear.ciudad!=="" && this.proveedorCrear.apellido2!=="" && this.proveedorCrear.cp!=="" &&
        this.proveedorCrear.direccion!=="" && this.proveedorCrear.pais!=="" && this.proveedorCrear.provincia !==""; 
    console.log(this.activado);
  }

}
