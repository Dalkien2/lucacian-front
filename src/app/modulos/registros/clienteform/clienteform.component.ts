import { ClienteService } from 'src/app/core/servicios/cliente.service';
import { ClienteInt } from 'src/app/core/modelos/cliente-int';
import { Component, OnInit, Optional, Self, Host } from '@angular/core';
import { ClienteComponent } from '../cliente/cliente.component';

@Component({
  selector: 'app-clienteform',
  templateUrl: './clienteform.component.html',
  styleUrls: ['./clienteform.component.css']
})
export class ClienteformComponent implements OnInit {
  clienteCrear: ClienteInt= {cifCliente:null , nombre:"", apellido1:"", apellido2:"", nombreEmpresa:"", direccion:"" ,cp:"", ciudad:"",provincia:"", pais:"" };
  activado:boolean = false;
  constructor( @Optional() @Host() private cliente: ClienteComponent,
       private servClient: ClienteService) { }

  ngOnInit(): void {
    this.activado = true;
  }

  crearClient(){
    this.validaCampos();
    if(this.activado){
      console.log(JSON.stringify(this.clienteCrear));
      this.servClient.crearCliente(this.clienteCrear).subscribe((data)=> this.cliente.ActualizaLista());
    }else{
      setTimeout(()=> this.inactivo() ,3000) ;
    }
  }

  inactivo(){
    this.activado = true;
  }
  validaCampos(){
    console.log(this.activado);
    this.activado = this.clienteCrear.nombre!=="" && this.clienteCrear.apellido1!=="" &&
        this.clienteCrear.ciudad!=="" && this.clienteCrear.apellido2!=="" && this.clienteCrear.cp!=="" &&
        this.clienteCrear.direccion!=="" && this.clienteCrear.pais!=="" && this.clienteCrear.provincia !==""; 
    console.log(this.activado);
  }
}
