import { async } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { ClienteInt } from 'src/app/core/modelos/cliente-int';
import { ClienteService } from 'src/app/core/servicios/cliente.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  listClient: ClienteInt[] = [] ;
  constructor( private servCliente: ClienteService) { }

  ngOnInit(): void {
    this.servCliente.obtenerClientes().subscribe(
      (data) => {this.listClient = data as ClienteInt[];
         console.log("datos son:" + JSON.stringify(data))}, (error) => alert("contacte al administrador")
    );
  }

ActualizaLista(){
  this.servCliente.obtenerClientes().subscribe(
    (data) => {this.listClient = data as ClienteInt[];
       console.log("datos son:" + JSON.stringify(data))}, (error) => alert("contacte al administrador")
  );
  }
}
