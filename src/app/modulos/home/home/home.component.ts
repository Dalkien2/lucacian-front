import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  itemUno: string=  "En el primer elemento del menu, se encuentra todo el modulo de registro tanto para "+ 
  "clientes, articulos y proveedores, se observa el listado y formulario tanto para la creacion " +
  "como para la actualizacion de ciertos datos.";
  itemDos: string=  "En el segundo elemento del menu, se encuentra todo el modulo de ventas en donde se registra " +
  "la factura del cliente y el detalle de la venta donde se realiza la visualizacion de los datos "+
  "antes de efectuar la compra.";
  itemTres: string=  "En el tercer elemento del menu, se puede realizar de consulta de las compras por los clientes "+
  "con filtrado por mes, cliente y valor de compra."; 
  constructor() { }

  ngOnInit(): void {
  }

}
