import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  menu = {  accionMenu: false };
   icon : any ; 
  constructor() { }

  ngOnInit() {
      this.icon = document.getElementById("conti");
    this.cerrarMenu();

  }

  abrirMenu() {

    document.getElementById('mySidebar').style.width = '250px';
    document.getElementById('main').style.marginLeft = '250px';

  }

  cerrarMenu() {

    document.getElementById('mySidebar').style.width = '32.4px';
    document.getElementById('main').style.marginLeft = '32.4px';

  }

  accionarMenu(event) {

    this.icon.classList.toggle("change");
    if (this.menu.accionMenu === true) {

      this.cerrarMenu();
      this.menu.accionMenu = false;

    } else if (this.menu.accionMenu === false) {

      this.abrirMenu();
      this.menu.accionMenu = true;

    }

  }

}

