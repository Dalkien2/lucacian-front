import { DetfacturaInt } from './../../../core/modelos/detfactura-int';
import { Component, OnInit } from '@angular/core';
import { ClienteTot } from 'src/app/core/modelos/cliente-tot';
import { ClienteInt } from 'src/app/core/modelos/cliente-int';
import { FacturaInt } from 'src/app/core/modelos/factura-int';
import { ClienteService } from 'src/app/core/servicios/cliente.service';
import { ArticuloService } from 'src/app/core/servicios/articulo.service';
import { DetfacturaService } from 'src/app/core/servicios/detfactura.service';
import { FacturaService } from 'src/app/core/servicios/factura.service';
import { ArticuloInt } from 'src/app/core/modelos/articulo-int';
import { zip } from 'rxjs';

@Component({
  selector: 'app-gastos',
  templateUrl: './gastos.component.html',
  styleUrls: ['./gastos.component.css']
})
export class GastosComponent implements OnInit {

  listClient: ClienteTot[] =[];
  listClientFilt: ClienteTot[] =[];
  listClientes: ClienteInt[] = [];
  ListFacturas: FacturaInt[] = [];
  ListDetalleFeacts: DetfacturaInt[] = [];
  listArticulos: ArticuloInt[]= [];
  montoFiltra: number = 0; 
  constructor(private servCliente : ClienteService,
    private servArticulo: ArticuloService, 
    private servDetalle: DetfacturaService,
    private servFactura: FacturaService ) { }

  ngOnInit(): void {
    this.servCliente.obtenerClientes().subscribe( (data)=>{
        this.listClientes = data as ClienteInt[]; 
        });
      this.recuperaDatos();
      this.buscaDetalle();
      this.buscaArticulos();
    setTimeout(() => this.calctot(), 3000);
  }

  addclien(){
    for(let i = 0 ; i < this.listClientes.length; i++ ){
      let clie: ClienteTot = {
        cifCliente: this.listClientes[i].cifCliente,
        nombre:this.listClientes[i].nombre, 
        nombreEmpresa:  this.listClientes[i].nombreEmpresa,
        apellido1: this.listClientes[i].apellido1, 
        apellido2: this.listClientes[i].apellido2, 
        total: 0 
      }
      this.listClient.push(clie);
    }
  }
  recuperaDatos(){
    this.servFactura.obtenerFacturas().subscribe( (data) => 
      {
        this.ListFacturas = data as FacturaInt[]; 
        }
    );
  }

  buscaDetalle(){
    this.servDetalle.obtenerDetalles().subscribe((data)=> {
      this.ListDetalleFeacts = data as DetfacturaInt[];
    });
  }

  buscaArticulos(){
    this.servArticulo.obtenerArticulos().subscribe( (data) => {
      this.listArticulos = data as ArticuloInt[];
    }
    );
  }

  calctot(){
    this.addclien();
    let listfact: FacturaInt []=[];
    let listDetFac:  DetfacturaInt[] =[];
    let totalCom: number = 0;  
    this.listClient.forEach( (x) => {
      totalCom =0;
      listfact= [];
      listDetFac=[];
      this.ListFacturas.forEach(
        (y) =>{
          if(x.cifCliente===y.cifCliente){
            listfact.push(y);
          }
        });
        listfact.forEach( (y) => {
          this.ListDetalleFeacts.forEach((z) =>{ 
            if (y.numFactura === z.numfactura) {
              listDetFac.push(z);   
            }
          });
        });
        listDetFac.forEach((y)=> {
          let artic= this.listArticulos.find(z=> z.codArticulo=== y.codArticulo);
          if (artic!== undefined) {
            totalCom += (artic.precio * y.cantidad) ;
          }
        });
        x.total = totalCom;
    } );    
    this.listClientFilt = this.listClient;
    this.filtro();
  }
  filtro(){
    this.listClientFilt =[];
    this.listClientFilt = this.listClient.filter((x) => x.total >= this.montoFiltra );
    console.log(JSON.stringify(this.listClientFilt));
  }
}
