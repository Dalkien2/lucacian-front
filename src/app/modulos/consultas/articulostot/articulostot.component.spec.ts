import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticulostotComponent } from './articulostot.component';

describe('ArticulostotComponent', () => {
  let component: ArticulostotComponent;
  let fixture: ComponentFixture<ArticulostotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticulostotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticulostotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
