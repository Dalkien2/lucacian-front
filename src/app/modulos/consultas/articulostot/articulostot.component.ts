import { ArtMax } from 'src/app/core/modelos/art-max';
import { Component, OnInit } from '@angular/core';
import { ArtMin } from 'src/app/core/modelos/art-min';
import { ArticuloService } from 'src/app/core/servicios/articulo.service';
import { DetfacturaService } from 'src/app/core/servicios/detfactura.service';
import { ProveedorService } from 'src/app/core/servicios/proveedor.service';
import { ArticuloInt } from 'src/app/core/modelos/articulo-int';
import { DetfacturaInt } from 'src/app/core/modelos/detfactura-int';
import { ProveedorInt } from 'src/app/core/modelos/proveedor-int';

@Component({
  selector: 'app-articulostot',
  templateUrl: './articulostot.component.html',
  styleUrls: ['./articulostot.component.css']
})
export class ArticulostotComponent implements OnInit {
  listArtMax: ArtMax[] = [];
  listArtMin: ArtMin[] = [];
  listArticulos: ArticuloInt[] = []; 
  listDetalles: DetfacturaInt[] = []; 
  listProveedores: ProveedorInt[] = [];
  constructor(private servArticulo: ArticuloService, 
              private servDetalle: DetfacturaService,
              private serProveedor: ProveedorService) { }

  ngOnInit(): void {
    this.servArticulo.obtenerArticulos().subscribe((data)=> this.listArticulos = data as ArticuloInt[] );
    this.serProveedor.obtieneProveedores().subscribe((data)=> this.listProveedores = data as ProveedorInt[]);
    this.servDetalle.obtenerDetalles().subscribe((data)=> this.listDetalles = data as DetfacturaInt[]);
    setTimeout(() =>  this.actualizaTablas(), 3000);
  }

  actualizaTablas(){
    let cont:number =0; 
    let detail: DetfacturaInt[]=[];
    this.listArticulos.forEach((x)=> {
      cont =0;
      detail=[];
       detail = this.listDetalles.filter((y)=> x.codArticulo === y.codArticulo);
       if(detail.length > 0){
         detail.forEach((y)=> cont += y.cantidad);
         let creaMax: ArtMax ={
           codigo: x.codArticulo, 
           nombre: x.nombreArticulo,
           cantidad: cont
         };
         this.listArtMax.push(creaMax);
       }else{
         let prov: ProveedorInt = this.listProveedores.find((y)=> y.cifProveedor === x.cifProveedor);
         let creaMin: ArtMin={
           codigo: x.codArticulo,
           nombre: x.nombreArticulo,
           proveedor: prov.nombre +" "+ prov.apellido1 +" "+ prov.apellido2
         }
         this.listArtMin.push(creaMin);
       }
    })
  }
}
