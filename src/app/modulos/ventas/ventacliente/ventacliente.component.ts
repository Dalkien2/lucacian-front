import { DetfacturaInt } from './../../../core/modelos/detfactura-int';
import { FacturaInt } from './../../../core/modelos/factura-int';
import { ClienteService } from 'src/app/core/servicios/cliente.service';
import { Component, OnInit, Optional } from '@angular/core';
import { ClienteInt } from 'src/app/core/modelos/cliente-int';
import { ArticuloService } from 'src/app/core/servicios/articulo.service';
import { ArticuloInt } from 'src/app/core/modelos/articulo-int';
import { DetfacturaService } from 'src/app/core/servicios/detfactura.service';
import { FacturaService } from 'src/app/core/servicios/factura.service';

@Component({
  selector: 'app-ventacliente',
  templateUrl: './ventacliente.component.html',
  styleUrls: ['./ventacliente.component.css']
})
export class VentaclienteComponent implements OnInit {

  listaClientes: ClienteInt[] = [];
  listaArticulos: ArticuloInt[] = [];
  listDetalles: DetfacturaInt[] = [];
  listFacturas: FacturaInt[]= []; 
  currentFact:number =0;
  facturaCrear: FacturaInt= { numFactura: null, fechaFactura: new Date(), cifCliente:0 }
  completo: boolean = false; 
  activado: boolean =  true; 
  constructor(private servCliente : ClienteService,
              private servArticulo: ArticuloService, 
              private servDetalle: DetfacturaService,
              private servFactura: FacturaService ) { }


  ngOnInit(): void {
    this.servCliente.obtenerClientes().subscribe((data) => this.listaClientes = data as ClienteInt[]
    , error => alert("contacte con el administrador"));
    this.servArticulo.obtenerArticulos().subscribe((data) => this.listaArticulos = data as ArticuloInt[]
    , error => alert("Contacte con el administrador"));
    this.servFactura.obtenerFacturas().subscribe( (data) => {this.listFacturas = data as FacturaInt[];
      this.currentFact = this.listFacturas.length +1;}); 
  }

  selCliente(e){
      this.facturaCrear.cifCliente = e.target.value; 
  }

  crearPedido(){
    this.valDatos();
    if(this.activado){
      console.log(JSON.stringify(this.listDetalles));
      console.log(JSON.stringify(this.facturaCrear));
      this.servFactura.creaFactura(this.facturaCrear).subscribe(()=> 
      this.altaListaDetalle() );
      //setTimeout(() => window.location.reload(), 4500);
    }else{
      setTimeout(() => this.inactive, 3000);
    }
  }

  inactive(){
    this.activado = true; 
  }
  altaListaDetalle(){
    this.servFactura.obtenerFacturas().subscribe((data)=> {this.listFacturas = data as FacturaInt[];
    this.insetDetalles();} );
  }

  insetDetalles(){
    let numf: number = this.listFacturas.length;
    this.listDetalles 
    for( let i = 0; i< this.listDetalles.length ; i++ )
    { this.listDetalles[i].numfactura= numf;
      this.listDetalles[i].numdetallefactura = i +1;
       this.servDetalle.crearDetalle(this.listDetalles[i]).subscribe((data)=> console.log("ok "+i)); 
      }
    
  }

  valDatos(){
    this.activado = this.facturaCrear.cifCliente!== 0 ;    
  }

}
