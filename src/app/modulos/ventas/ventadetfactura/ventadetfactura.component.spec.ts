import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentadetfacturaComponent } from './ventadetfactura.component';

describe('VentadetfacturaComponent', () => {
  let component: VentadetfacturaComponent;
  let fixture: ComponentFixture<VentadetfacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentadetfacturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentadetfacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
