import { DetfacturaInt } from './../../../core/modelos/detfactura-int';
import { Component, OnInit, Optional, Host } from '@angular/core';
import { ArticuloInt } from 'src/app/core/modelos/articulo-int';
import { ArticuloService } from 'src/app/core/servicios/articulo.service';
import { VentaclienteComponent } from '../ventacliente/ventacliente.component';

@Component({
  selector: 'app-ventadetfactura',
  templateUrl: './ventadetfactura.component.html',
  styleUrls: ['./ventadetfactura.component.css']
})
export class VentadetfacturaComponent implements OnInit {

  listDetFactura: DetfacturaInt[] =[]; 
  listaArticulos: ArticuloInt[]=[];
  detCrea: DetfacturaInt = {numfactura:0, numdetallefactura:0, codArticulo:0, cantidad:0,  porcentajeGanacia:0 };
  activado:boolean = true;
  valBase:number = 0;
  currenFact:number = 0;  
  constructor(private servArticulo: ArticuloService, 
              @Optional() @Host() private ventaCliente: VentaclienteComponent) { }

  ngOnInit(): void {
    this.servArticulo.obtenerArticulos().subscribe( (data) => this.listaArticulos = data as ArticuloInt[], 
        (error) => alert("contacte con el administrador") );
        this.detCrea.numdetallefactura= this.listDetFactura.length +1;
        this.currenFact= this.ventaCliente.currentFact;
  }

  adicionaFactura(){
    this.valdatos();
    if(this.activado){
      this.listDetFactura.push(this.detCrea);
      this.detCrea = {numfactura:0, numdetallefactura:0, codArticulo:0, cantidad:0,  porcentajeGanacia:0 };
      this.detCrea.numdetallefactura= this.listDetFactura.length +1;
      let seleccion:any = document.getElementById("articulo") ;
      seleccion.value = 0; 
      this.ventaCliente.listDetalles = []; 
      this.ventaCliente.listDetalles = this.listDetFactura;
    }else{
      setTimeout(() => this.inactiva(), 3000);
    }
  }

  valdatos(){
    this.activado = this.detCrea.cantidad!== 0 && this.detCrea.codArticulo !== 0 && 
        this.detCrea.numdetallefactura !==0 && this.detCrea.porcentajeGanacia !==0 ;  
  }

  inactiva(){
    this.activado = true; 
  }
  selArticulo(e){
    this.detCrea.codArticulo = e.target.value; 
  }
}
