export interface ProveedorInt {
    cifProveedor?: number;
    nombre?: string;
    apellido1?: string;
    apellido2?: string; 
    nombreEmpresa?: string;
    direccion?: string;
    cp?: string;
    ciudad?: string;
    provincia?: string;
    pais?: string;
}
