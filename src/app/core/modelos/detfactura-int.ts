export interface DetfacturaInt {
    numfactura?: number;
    numdetallefactura?: number;
    codArticulo?: number;
    cantidad?: number;
    porcentajeGanacia?: number;
}
