export interface ArticuloInt {
    codArticulo?: number; 
    cifProveedor?: number;
    nombreArticulo?: string;
    caracteristica?: string;
    precio?: number;
}
