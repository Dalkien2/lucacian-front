import { Timestamp } from 'rxjs';

export interface FacturaInt {
    numFactura?: number;
    fechaFactura?: Date;
    cifCliente?: number;
}
