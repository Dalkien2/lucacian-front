import { ClienteInt } from 'src/app/core/modelos/cliente-int';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  endpoint: string = "http://localhost:8080/api/Cliente";
  constructor(private http: HttpClient) { }

  obtenerClientes(): Observable<ClienteInt>{
    return this.http.get(this.endpoint);
  }
  
  crearCliente(cliente: ClienteInt){
    return this.http.post(this.endpoint,cliente);
  }
}
