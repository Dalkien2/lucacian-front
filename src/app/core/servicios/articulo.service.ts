import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ArticuloInt } from '../modelos/articulo-int';

@Injectable({
  providedIn: 'root'
})
export class ArticuloService {

  constructor(private http: HttpClient) { }

  endpoint: string = "http://localhost:8080/api/Articulo";

  obtenerArticulos(){
    return this.http.get(this.endpoint);
  }

  crearArticulo(articulo: ArticuloInt){
    return this.http.post(this.endpoint,articulo)
  }
}
