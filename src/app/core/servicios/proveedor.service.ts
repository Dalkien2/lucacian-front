import { ProveedorInt } from './../modelos/proveedor-int';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  constructor(private http: HttpClient) { }
  endpoint: string = "http://localhost:8080/api/Proveedor";

  obtieneProveedores(){
    return this.http.get(this.endpoint);
  }

  creaProveedor( prveedor:ProveedorInt){
    return this.http.post(this.endpoint,prveedor);
  }
}
