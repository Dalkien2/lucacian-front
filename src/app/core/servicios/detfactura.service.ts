import { DetfacturaInt } from './../modelos/detfactura-int';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DetfacturaService {
  endpoint:string = "http://localhost:8080/api/DetalleFactura";
  constructor(private http: HttpClient) { }

  obtenerDetallesFact(numfact: number){
    return this.http.get(this.endpoint+"/"+numfact);
  }

  obtenerDetalles(){
    return this.http.get(this.endpoint);
  }

  crearDetalle( detalle:DetfacturaInt){
    return this.http.post(this.endpoint,detalle);
  }
}
