import { FacturaInt } from './../modelos/factura-int';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  endpoint: string = "http://localhost:8080/api/Factura";
  constructor(private http: HttpClient) { }

  obtenerFacturas(){
    return this.http.get(this.endpoint);
  }

  creaFactura(fact: FacturaInt){
    return this.http.post(this.endpoint,fact);
  }
}
