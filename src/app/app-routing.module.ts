import { VentaclienteComponent } from './modulos/ventas/ventacliente/ventacliente.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './modulos/home/home/home.component';
import { ClienteComponent } from './modulos/registros/cliente/cliente.component';
import { ArticulosComponent } from './modulos/registros/articulos/articulos.component';
import { ProveedoresComponent } from './modulos/registros/proveedores/proveedores.component';
import { GastosComponent } from './modulos/consultas/gastos/gastos.component';
import { ArticulostotComponent } from './modulos/consultas/articulostot/articulostot.component';


const routes: Routes = [
  {path: "",component: HomeComponent},
  //Acceso a Registro por cualquier concepto
  {path: "registros/clientes", component: ClienteComponent},
  {path: "registros/articulos", component: ArticulosComponent},
  {path: "registros/proveedores", component: ProveedoresComponent},
  //Acceso a Ventas
  {path: "ventas/cliente", component: VentaclienteComponent},
  //Acceso a consultas
  {path: "consulta/gastos", component: GastosComponent},
  {path: "consulta/articulos", component: ArticulostotComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
